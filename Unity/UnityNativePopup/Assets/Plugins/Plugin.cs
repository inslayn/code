// Plugin.cs
// 
// Created by Nick McVroom-Amoakohene <nick@nickmcvroom.com>.
// 
// Copyright (c) 2012 inslayn in ya membrane. http://inslayn.co. All rights reserved
using System.Runtime.InteropServices;
using UnityEngine;
using System;

public class Plugin
{
#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void showAlert(string alertData);
	
	public static void ShowAlert(string heading, string message, int numberOfButtons, string[] buttonNames, string[] buttonDelegateClasses, string[] buttonDelegateMethods) {
		string ad = "";
		string names = "", classes = "", methods = "";
		
		for (int i = 0; i < numberOfButtons; i++) {
			names 	+= buttonNames[i]; 
			classes += buttonDelegateClasses[i]; 
			methods += buttonDelegateMethods[i];
			
			if(i < numberOfButtons - 1) {
				names 	+= ";";
				classes += ";";
				methods += ";";
			}
		}
		
		// Create the alert data string
		ad = heading + ":" + message + ":" + numberOfButtons + ":" + names + ":" + classes + ":" + methods;
		
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			showAlert(ad);
		}
		else
			Debug.Log(ad);
	}
#endif
}