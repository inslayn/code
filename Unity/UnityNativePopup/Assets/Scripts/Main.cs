// Main.cs
// 
// Created by Nick McVroom-Amoakohene <nick@nickmcvroom.com>.
// 
// Copyright (c) 2012 inslayn in ya membrane. http://inslayn.co. All rights reserved
using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {

	public void TwoButtonOK() {
		Plugin.ShowAlert("Two Button", "OK was pressed!", 1, new string[] { "Dismiss" }, new string[] { string.Empty }, new string[] { string.Empty });
	}
	
	public void ThreeButtonAbort() {
		Plugin.ShowAlert("Three Button", "Abort was pressed!", 1, new string[] { "Dismiss" }, new string[] { string.Empty }, new string[] { string.Empty });
	}
	
	public void ThreeButtonRetry() {
		Plugin.ShowAlert("Three Button", "Retry was pressed!", 1, new string[] { "Dismiss" }, new string[] { string.Empty }, new string[] { string.Empty });
	}

	void OnGUI() {
		GUILayout.BeginVertical();

		GUILayout.Space(50f);

		GUILayout.BeginHorizontal();
		
		GUILayout.Space(50f);

		if(GUILayout.Button("Show One Button Alert")) {
			// passing an empty string as to the callback params will just dismiss a popup
			Plugin.ShowAlert("Alert View", "I'm a one-button alert!", 1, new string[] { "Dismiss" }, new string[] { string.Empty }, new string[] { string.Empty });
		}
		
		GUILayout.Space(50f);
		
		if(GUILayout.Button("Show Two Button Alert")) {
			Plugin.ShowAlert("Alert View", "I'm a two-button alert!", 2, new string[] { "OK", "Cancel" }, new string[] { "Main", string.Empty }, new string[] { "TwoButtonOK", string.Empty });
		}
		
		GUILayout.Space(50f);
		
		if(GUILayout.Button("Show Three Button Alert")) {
			Plugin.ShowAlert("Action Sheet", "I'm a three-button alert!", 3, new string[] { "Abort", "Retry", "Ignore" }, new string[] { "Main", "Main", string.Empty }, new string[] { "ThreeButtonAbort", "ThreeButtonRetry", string.Empty });
		}
		
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
}
