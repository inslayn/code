//
//  UnityAlertView.h
//  Unity-iPhone
//
//  Created by Nick McVroom.
//  Copyright (c) 2012 inslayn in ya membrane. All rights reserved.
//
//  Enables callback methods to be attached to Cocoa's UIAlertView.

#import <Foundation/Foundation.h>

@interface UnityAlertView : NSObject <UIAlertViewDelegate, UIActionSheetDelegate> {
    NSArray* callbackObjects;
    NSArray* callbackMethods;
    
    UIAlertView* unityAlertView;
    UIActionSheet* unityActionSheet;
}

@property(nonatomic, retain) NSArray* callbackObjects;
@property(nonatomic, retain) NSArray* callbackMethods;

-(void)showUnityAlert:(NSString *)heading message:(NSString *)msg buttonCount:(int)count buttonStrings:(NSArray *)btnStrings buttonDelegateClasses:(NSArray *)btnDelCls buttonDelegateMethods:(NSArray *)btnDelMtds view:(UIView*)view;

@end