//
//  AppController+UnityAlertViews.m
//  Unity-iPhone
//
//  Created by Nick McVroom.
//  Copyright (c) 2012 inslayn in ya membrane. All rights reserved.
//

#import "AppController.h"
#import "UnityAlertView.h"

#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <Availability.h>

@interface AppController (UnityAlertViews) <UIApplicationDelegate> {
}
@end

@implementation AppController (UnityAlertViews)

extern "C" {
    void showAlert(const char* alertData);
}

void showAlert(const char* alertData) {
    NSString *tokens    = [NSString stringWithUTF8String:alertData];
    NSArray *chunks     = [tokens componentsSeparatedByString: @":"];
    
    NSArray* btnNames   = [[chunks objectAtIndex:3] componentsSeparatedByString:@";"];
    NSArray* btnCls     = [[chunks objectAtIndex:4] componentsSeparatedByString:@";"];
    NSArray* btnMtds    = [[chunks objectAtIndex:5] componentsSeparatedByString:@";"];
    
    UnityAlertView* alert = [UnityAlertView alloc];
    
    [alert showUnityAlert:[chunks objectAtIndex:0] message:[chunks objectAtIndex:1] buttonCount:[[chunks objectAtIndex:2] intValue] buttonStrings:btnNames buttonDelegateClasses:btnCls buttonDelegateMethods:btnMtds view:[AppController getUnityView]];
}

@end