//
//  UnityAlertView.m
//  Unity-iPhone
//
//  Created by Nick McVroom.
//  Copyright (c) 2012 inslayn in ya membrane. All rights reserved.
//
//  Enables callback methods to be attached to Cocoa's UIAlertView, switches between an alert view
//  and an action sheet depending on the number of buttons required.

#import "UnityAlertView.h"

@implementation UnityAlertView

@synthesize callbackObjects;
@synthesize callbackMethods;

-(void)showUnityAlert:(NSString *)heading message:(NSString *)msg buttonCount:(int)count buttonStrings:(NSArray *)btnStrings buttonDelegateClasses:(NSArray *)btnDelCls buttonDelegateMethods:(NSArray *)btnDelMtds view:(UIView *)view {
    
    self.callbackObjects = [NSArray arrayWithArray:btnDelCls];
    self.callbackMethods = [NSArray arrayWithArray:btnDelMtds];
    
    if(count > 2) { // Show an action sheet if there are more than two buttons
        unityActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"%@\n%@", heading, msg]
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
        
        for (int i = 0; i < count; i++) {
            [unityActionSheet addButtonWithTitle:[btnStrings objectAtIndex:i]];
        }
        
        [unityActionSheet showInView:view];
    }
    else {
        unityAlertView = [[UIAlertView alloc] initWithTitle:heading message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        
        for (int i = 0; i < count; i++) {
            [unityAlertView addButtonWithTitle:[btnStrings objectAtIndex:i]];
        }
        
        [unityAlertView show];
    }
}

-(void) alertView: (UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* callbackObject = [self.callbackObjects objectAtIndex:buttonIndex];
    NSString* callbackMethod = [self.callbackMethods objectAtIndex:buttonIndex];
    
    if(![callbackObject isEqualToString:@""] && ![callbackMethod isEqualToString:@""])
        UnitySendMessage([callbackObject UTF8String], [callbackMethod UTF8String], "");
    
    [callbackObject release];
    [callbackMethod release];
    
    [unityAlertView release];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* callbackObject = [self.callbackObjects objectAtIndex:buttonIndex];
    NSString* callbackMethod = [self.callbackMethods objectAtIndex:buttonIndex];
    
    if(![callbackObject isEqualToString:@""] && ![callbackMethod isEqualToString:@""])
        UnitySendMessage([callbackObject UTF8String], [callbackMethod UTF8String], "");
    
    [callbackObject release];
    [callbackMethod release];
    
    [unityActionSheet release];
}

-(void) dealloc {
    [self.callbackObjects release];
    [self.callbackMethods release];
    [super dealloc];
}

@end