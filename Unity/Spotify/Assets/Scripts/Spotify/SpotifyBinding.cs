using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

/// <summary>
/// This class contains the function calls to enable Unity to call native Spotify methods.
/// </summary>
public static class SpotifyBinding {
	
	internal static object Mutex = new object();
	
	#region Spotify Constants
	
	public const int kSpotifyApiVersion = 12;
	
	#endregion
	
	#region Enums
	
	/// <summary>
	/// Error codes.
	/// </summary>
	public enum sp_error {
		SP_ERROR_OK                        = 0,  ///< No errors encountered
		SP_ERROR_BAD_API_VERSION           = 1,  ///< The library version targeted does not match the one you claim you support
		SP_ERROR_API_INITIALIZATION_FAILED = 2,  ///< Initialization of library failed - are cache locations etc. valid?
		SP_ERROR_TRACK_NOT_PLAYABLE        = 3,  ///< The track specified for playing cannot be played
		SP_ERROR_BAD_APPLICATION_KEY       = 5,  ///< The application key is invalid
		SP_ERROR_BAD_USERNAME_OR_PASSWORD  = 6,  ///< Login failed because of bad username and/or password
		SP_ERROR_USER_BANNED               = 7,  ///< The specified username is banned
		SP_ERROR_UNABLE_TO_CONTACT_SERVER  = 8,  ///< Cannot connect to the Spotify backend system
		SP_ERROR_CLIENT_TOO_OLD            = 9,  ///< Client is too old, library will need to be updated
		SP_ERROR_OTHER_PERMANENT           = 10, ///< Some other error occurred, and it is permanent (e.g. trying to relogin will not help)
		SP_ERROR_BAD_USER_AGENT            = 11, ///< The user agent string is invalid or too long
		SP_ERROR_MISSING_CALLBACK          = 12, ///< No valid callback registered to handle events
		SP_ERROR_INVALID_INDATA            = 13, ///< Input data was either missing or invalid
		SP_ERROR_INDEX_OUT_OF_RANGE        = 14, ///< Index out of range
		SP_ERROR_USER_NEEDS_PREMIUM        = 15, ///< The specified user needs a premium account
		SP_ERROR_OTHER_TRANSIENT           = 16, ///< A transient error occurred.
		SP_ERROR_IS_LOADING                = 17, ///< The resource is currently loading
		SP_ERROR_NO_STREAM_AVAILABLE       = 18, ///< Could not find any suitable stream to play
		SP_ERROR_PERMISSION_DENIED         = 19, ///< Requested operation is not allowed
		SP_ERROR_INBOX_IS_FULL             = 20, ///< Target inbox is full
		SP_ERROR_NO_CACHE                  = 21, ///< Cache is not enabled
		SP_ERROR_NO_SUCH_USER              = 22, ///< Requested user does not exist
		SP_ERROR_NO_CREDENTIALS            = 23, ///< No credentials are stored
		SP_ERROR_NETWORK_DISABLED          = 24, ///< Network disabled
		SP_ERROR_INVALID_DEVICE_ID         = 25, ///< Invalid device ID
		SP_ERROR_CANT_OPEN_TRACE_FILE      = 26, ///< Unable to open trace file
		SP_ERROR_APPLICATION_BANNED        = 27, ///< This application is no longer allowed to use the Spotify service
		SP_ERROR_OFFLINE_TOO_MANY_TRACKS   = 31, ///< Reached the device limit for number of tracks to download
		SP_ERROR_OFFLINE_DISK_CACHE        = 32, ///< Disk cache is full so no more tracks can be downloaded to offline mode
		SP_ERROR_OFFLINE_EXPIRED           = 33, ///< Offline key has expired, the user needs to go online again
		SP_ERROR_OFFLINE_NOT_ALLOWED       = 34, ///< This user is not allowed to use offline mode
		SP_ERROR_OFFLINE_LICENSE_LOST      = 35, ///< The license for this device has been lost. Most likely because the user used offline on three other device
		SP_ERROR_OFFLINE_LICENSE_ERROR     = 36, ///< The Spotify license server does not respond correctly
		SP_ERROR_LASTFM_AUTH_ERROR         = 39, ///< A LastFM scrobble authentication error has occurred
		SP_ERROR_INVALID_ARGUMENT          = 40, ///< An invalid argument was specified
		SP_ERROR_SYSTEM_FAILURE            = 41, ///< An operating system error
	}
	
	/// <summary>
	/// Connection state.
	/// </summary>
	public enum sp_connectionstate {
		SP_CONNECTION_STATE_LOGGED_OUT   	= 0, ///< User not yet logged in
		SP_CONNECTION_STATE_LOGGED_IN    	= 1, ///< Logged in against a Spotify access point
		SP_CONNECTION_STATE_DISCONNECTED 	= 2, ///< Was logged in, but has now been disconnected
		SP_CONNECTION_STATE_UNDEFINED    	= 3, ///< The connection state is undefined
		SP_CONNECTION_STATE_OFFLINE			= 4  ///< Logged in in offline mode
	}
	
	/// <summary>
	/// Current connection type set using sp_session_set_connection_type().
	/// </summary>
	public enum sp_connection_type {
		SP_CONNECTION_TYPE_UNKNOWN        = 0, ///< Connection type unknown (Default)
		SP_CONNECTION_TYPE_NONE           = 1, ///< No connection
		SP_CONNECTION_TYPE_MOBILE         = 2, ///< Mobile data (EDGE, 3G, etc)
		SP_CONNECTION_TYPE_MOBILE_ROAMING = 3, ///< Roamed mobile data (EDGE, 3G, etc)
		SP_CONNECTION_TYPE_WIFI           = 4, ///< Wireless connection
		SP_CONNECTION_TYPE_WIRED          = 5, ///< Ethernet cable, etc
	}
	
	/// <summary> 
	/// Connection rules, bitwise OR of flags
	/// The default is SP_CONNECTION_RULE_NETWORK | SP_CONNECTION_RULE_ALLOW_SYNC
	/// </summary>
	public enum sp_connection_rules {
		SP_CONNECTION_RULE_NETWORK                = 0x1, ///< Allow network traffic. When not set libspotify will force itself into offline mode
		SP_CONNECTION_RULE_NETWORK_IF_ROAMING     = 0x2, ///< Allow network traffic even if roaming
		SP_CONNECTION_RULE_ALLOW_SYNC_OVER_MOBILE = 0x4, ///< Set to allow syncing of offline content over mobile connections
		SP_CONNECTION_RULE_ALLOW_SYNC_OVER_WIFI   = 0x8, ///< Set to allow syncing of offline content over WiFi
	}
	
	/// <summary>
	/// Sample type.
	/// </summary>
	public enum sp_sampletype {
  		SP_SAMPLETYPE_INT16_NATIVE_ENDIAN 	= 0, ///< 16-bit signed integer samples
	}

	/// <summary>
	/// Link types.
	/// </summary>
	public enum sp_linktype {
		SP_LINKTYPE_INVALID  	= 0, ///< Link type not valid - default until the library has parsed the link, or when parsing failed
		SP_LINKTYPE_TRACK    	= 1, ///< Link type is track
		SP_LINKTYPE_ALBUM    	= 2, ///< Link type is album
		SP_LINKTYPE_ARTIST   	= 3, ///< Link type is artist
		SP_LINKTYPE_SEARCH   	= 4, ///< Link type is search
		SP_LINKTYPE_PLAYLIST 	= 5, ///< Link type is playlist
		SP_LINKTYPE_PROFILE  	= 6, ///< Link type is profile
		SP_LINKTYPE_STARRED  	= 7, ///< Link type is starred
		SP_LINKTYPE_LOCALTRACK  = 8, ///< Link type is a local file
		SP_LINKTYPE_IMAGE 		= 9, ///< Link type is an image
	}
	
	/// <summary>
	/// Image format.
	/// </summary>
	public enum sp_imageformat {
		SP_IMAGE_FORMAT_UNKNOWN = -1, 	///< Unknown image format
		SP_IMAGE_FORMAT_JPEG   	= 0,   	///< JPEG image
	}
	
	/// <summary>
	/// Image size.
	/// </summary>
	public enum sp_image_size {
		SP_IMAGE_SIZE_NORMAL	= 0, ///< Normal image size
		SP_IMAGE_SIZE_SMALL		= 1, ///< Small image size
		SP_IMAGE_SIZE_LARGE		= 2, ///< Large image size
	}

	/// <summary>
	/// Album types.
	/// </summary>
	public enum sp_albumtype {
		SP_ALBUMTYPE_ALBUM       = 0, ///< Normal album
		SP_ALBUMTYPE_SINGLE      = 1, ///< Single
		SP_ALBUMTYPE_COMPILATION = 2, ///< Compilation
		SP_ALBUMTYPE_UNKNOWN     = 3, ///< Unknown type
	}

	/// <summary>
	/// Bitrate definitions for music streaming.
	/// </summary>
    public enum sp_bitrate {
		SP_BITRATE_160k      = 0, ///< Bitrate 160kbps
		SP_BITRATE_320k      = 1, ///< Bitrate 320kbps
		SP_BITRATE_96k       = 2, ///< Bitrate 96kbps
    }
	
	/// <summary>
	/// Playlist types.
	/// </summary>
	public enum sp_playlist_type {
		SP_PLAYLIST_TYPE_PLAYLIST     = 0, ///< A normal playlist.
		SP_PLAYLIST_TYPE_START_FOLDER = 1, ///< Marks a folder starting point,
		SP_PLAYLIST_TYPE_END_FOLDER   = 2, ///< and ending point.
		SP_PLAYLIST_TYPE_PLACEHOLDER  = 3, ///< Unknown entry.
	}
	
	/// <summary>
	/// Playlist offline status.
	/// </summary>
	public enum sp_playlist_offline_status {
		SP_PLAYLIST_OFFLINE_STATUS_NO          = 0, ///< Playlist is not offline enabled
		SP_PLAYLIST_OFFLINE_STATUS_YES         = 1, ///< Playlist is synchronized to local storage
		SP_PLAYLIST_OFFLINE_STATUS_DOWNLOADING = 2, ///< This playlist is currently downloading. Only one playlist can be in this state any given time
		SP_PLAYLIST_OFFLINE_STATUS_WAITING     = 3, ///< Playlist is queued for download
	}
	
	/// <summary>
	/// Search types.
	/// </summary>
	public enum sp_search_type {
		SP_SEARCH_STANDARD  = 0,
		SP_SEARCH_SUGGEST 	= 1,
	}
	
	/// <summary>
	/// Controls the type of data that will be included in artist browse queries.
	/// </summary>
	public enum sp_artist_browse_type {
		SP_ARTISTBROWSE_FULL,         /**< All information except tophit tracks
		               					This mode is deprecated and will removed in a future release */
		SP_ARTISTBROWSE_NO_TRACKS,    /**< Only albums and data about them, no tracks.
		         						In other words, sp_artistbrowse_num_tracks() will return 0 */
		SP_ARTISTBROWSE_NO_ALBUMS,    /**< Only return data about the artist (artist name, similar artist
										biography, etc
										No tracks or album will be abailable.
										sp_artistbrowse_num_tracks() and sp_artistbrowse_num_albums()
										will both return 0 */
	}
	
	/// <summary>
	/// Social provider.
	/// </summary>
	public enum sp_social_provider {
		SP_SOCIAL_PROVIDER_SPOTIFY,
		SP_SOCIAL_PROVIDER_FACEBOOK,
		SP_SOCIAL_PROVIDER_LASTFM,
	}
	
	/// <summary>
	/// Scrobbling state.
	/// </summary>
	public enum sp_scrobbling_state {
		SP_SCROBBLING_STATE_USE_GLOBAL_SETTING    = 0,
		SP_SCROBBLING_STATE_LOCAL_ENABLED         = 1,
		SP_SCROBBLING_STATE_LOCAL_DISABLED        = 2,
		SP_SCROBBLING_STATE_GLOBAL_ENABLED        = 3,
		SP_SCROBBLING_STATE_GLOBAL_DISABLED       = 4,
	}
	
	/// <summary>
	/// Track availability.
	/// </summary>
	public enum sp_availability {
		SP_TRACK_AVAILABILITY_UNAVAILABLE 		= 0, ///< Track is not available
		SP_TRACK_AVAILABILITY_AVAILABLE   		= 1, ///< Track is available and can be played
		SP_TRACK_AVAILABILITY_NOT_STREAMABLE 	= 2, ///< Track can not be streamed using this account
		SP_TRACK_AVAILABILITY_BANNED_BY_ARTIST 	= 3, ///< Track not available on artist's reqeust
	}
	
	/// <summary>
	/// Track offline status.
	/// </summary>
	public enum sp_track_offline_status {
		SP_TRACK_OFFLINE_NO             = 0, ///< Not marked for offline
		SP_TRACK_OFFLINE_WAITING        = 1, ///< Waiting for download
		SP_TRACK_OFFLINE_DOWNLOADING    = 2, ///< Currently downloading
		SP_TRACK_OFFLINE_DONE           = 3, ///< Downloaded OK and can be played
		SP_TRACK_OFFLINE_ERROR          = 4, ///< Error during download
		SP_TRACK_OFFLINE_DONE_EXPIRED   = 5, ///< Downloaded OK but not playable due to expiery
		SP_TRACK_OFFLINE_LIMIT_EXCEEDED = 6, ///< Waiting because device have reached max number of allowed tracks
		SP_TRACK_OFFLINE_DONE_RESYNC    = 7, ///< Downloaded OK and available but scheduled for re-download
	}
	
	/// <summary>
	/// User relation type.
	/// </summary>
	public enum sp_relation_type {
		SP_RELATION_TYPE_UNKNOWN 		= 0,   ///< Not yet known
		SP_RELATION_TYPE_NONE 			= 1,   ///< No relation
		SP_RELATION_TYPE_UNIDIRECTIONAL = 2,   ///< The currently logged in user is following this uer
		SP_RELATION_TYPE_BIDIRECTIONAL 	= 3,   ///< Bidirectional friendship established
	}
	
	/// <summary>
	/// Toplist type.
	/// </summary>
	public enum sp_toplisttype {
		SP_TOPLIST_TYPE_ARTISTS = 0, ///< Top artists
		SP_TOPLIST_TYPE_ALBUMS  = 1, ///< Top albums
		SP_TOPLIST_TYPE_TRACKS  = 2, ///< Top tracks
	}
	
	/// <summary>
	/// Toplist region.
	/// </summary>
	public enum sp_toplistregion {
		SP_TOPLIST_REGION_EVERYWHERE = 0, ///< Global toplist
		SP_TOPLIST_REGION_USER = 1,       ///< Toplist for a given user
	}
	
    #endregion
	
	#region Strings

    internal static string GetString(IntPtr ptr, string defaultValue) {
        if (ptr == IntPtr.Zero)
            return defaultValue;

		List<byte> bytes = new List<byte>();            
        
		byte read = 0;
        
		do {
            read = Marshal.ReadByte(ptr, bytes.Count);                
            bytes.Add(read);
        }
        while (read != 0);

        if (bytes.Count > 0)
            return Encoding.UTF8.GetString(bytes.ToArray(), 0, bytes.Count - 1);
        else
            return string.Empty;
    }

    #endregion
	
    #region Error handling

	#if UNITY_IPHONE
	[DllImport("__Internal")]
	#else
	[DllImport("libspotify")]
	#endif
	public static extern string sp_error_message(sp_error error);

	#endregion

	#region Session handling

	#region Structs

	internal struct sp_session_config {
		internal int api_version;
		internal string cache_location;
		internal string settings_location;
		internal IntPtr application_key;
		internal int application_key_size;
		internal string user_agent;
		internal IntPtr callbacks;
		internal IntPtr userdata;
	}		

	internal struct sp_session_callbacks {
		internal IntPtr logged_in;
		internal IntPtr logged_out;
		internal IntPtr metadata_updated;
		internal IntPtr connection_error;
		internal IntPtr message_to_user;
		internal IntPtr notify_main_thread;
		internal IntPtr music_delivery;
		internal IntPtr play_token_lost;
		internal IntPtr log_message;
		internal IntPtr end_of_track;
		internal IntPtr streaming_error;
		internal IntPtr userinfo_updated;
	}
	
	internal struct sp_audioformat {
		internal int sample_type;
		internal int sample_rate;
		internal int channels;
	}

	#endregion

	#region Methods
	
	#endregion
	
	#endregion
}
