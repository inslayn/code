using UnityEngine;
using System.Collections.Generic;

public class Main : MonoBehaviour {

	List<VertletRope> ropes = new List<VertletRope>();

	void Update() {
		if(Input.GetMouseButtonDown(1)) {

			VertletRope vr = new VertletRope(Vector2.zero, new Vector2(10, 10));
			ropes.Add(vr);
		}
	}

	void FixedUpdate() {
		for(int i = 0, ni = ropes.Count; i < ni; i++) {
			ropes[i].FixedUpdate();
		}
	}
}