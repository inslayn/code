﻿using UnityEngine;
using System.Collections;

public class VertletPoint {
	public float x, y;
	float old_x, old_y;

	/// <summary>
	/// Gets the VertletPoint as a Vector2.
	/// </summary>
	public Vector2 AsVector2 { get {  return new Vector2(x, y); } }
	
	/// <summary>
	/// Gets the VertletPoint as a Vector3.
	/// </summary>
	public Vector3 AsVector3 { get {  return new Vector3(x, y); } }

	GameObject go;

	/// <summary>
	/// Toggles debug drawing.
	/// </summary>
	public bool DebugDrawingEnabled = true;

	/// <summary>
	/// Initializes a new instance of the <see cref="VertletPoint"/> class.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public VertletPoint(float x, float y) {
		go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		SetPosition(x, y);
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="VertletPoint"/> class.
	/// </summary>
	/// <param name="coordinates">Vector2 which defines the x and y coordinates.</param>
	public VertletPoint(Vector2 coordinates) : this(coordinates.x, coordinates.y) {
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="VertletPoint"/> class.
	/// </summary>
	/// <param name="coordinates">Vector3 which defines the x and y coordinates. z coordinate is ignored.</param>
	public VertletPoint(Vector3 coordinates) : this(coordinates.x, coordinates.y) {
	}

	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="new_x">New_x.</param>
	/// <param name="new_y">New_y.</param>
	public void SetPosition(float new_x, float new_y) {
		x = old_x = new_x;
		y = old_y = new_y;
		
		go.name = string.Format("Vertlet Point [{0}, {1}]", x, y);

		if(DebugDrawingEnabled) DebugDraw();
	}

	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="newPos">New position.</param>
	public void SetPosition(Vector2 newPos) {
		SetPosition(newPos.x, newPos.y);
	}

	/// <summary>
	/// Sets the position.
	/// </summary>
	/// <param name="newPos">New position. z is ignored.</param>
	public void SetPosition(Vector3 newPos) {
		SetPosition(newPos.x, newPos.y);
	}

	/// <summary>
	/// Updates the position.
	/// </summary>
	public void UpdatePosition() {
		float temp_x = x;
		float temp_y = y;

		x += x - old_x;
		y += y - old_y;

		old_x = temp_x;
		old_y = temp_y;

		go.name = string.Format("Vertlet Point [{0}, {1}]", x, y);

		if(DebugDrawingEnabled) DebugDraw();
	}

	/// <summary>
	/// Cleanup this instance.
	/// </summary>
	public void Cleanup() {
		GameObject.Destroy(go);
	}

	void DebugDraw() {
		go.transform.position = AsVector3;
	}
}
