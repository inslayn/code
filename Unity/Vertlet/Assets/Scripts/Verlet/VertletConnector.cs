﻿using UnityEngine;
using System.Collections;

public class VertletConnector {
	public VertletPoint a, b;

	float hypotenuse;
	
	/// <summary>
	/// Toggles debug drawing.
	/// </summary>
	public bool DebugDrawingEnabled = true;

	/// <summary>
	/// Initializes a new instance of the <see cref="VertletConnector"/> class.
	/// </summary>
	/// <param name="pointA">Point a.</param>
	/// <param name="pointB">Point b.</param>
	public VertletConnector(VertletPoint pointA, VertletPoint pointB) {
		a = pointA;
		b = pointB;

		hypotenuse = GetHypotenuse(pointA, pointB);
	}

	/// <summary>
	/// Updates the length of the connector.
	/// </summary>
	public void UpdateLength() {
		float delta_x = b.x - a.x;
		float delta_y = b.y - a.y;

		float hyp = GetHypotenuse(b, a);
		float delta = hypotenuse - hyp;

		float x_offset = (delta * delta_x / hyp) * .5f;
		float y_offset = (delta * delta_y / hyp) * .5f;

		AdjustLength(x_offset, y_offset);

		if(DebugDrawingEnabled) DebugDraw();
	}

	/// <summary>
	/// Gets the hypotenuse between two points.
	/// </summary>
	/// <returns>The hypotenuse.</returns>
	/// <param name="pointA">Point a.</param>
	/// <param name="pointB">Point b.</param>
	float GetHypotenuse(VertletPoint pointA, VertletPoint pointB) {
		float delta_x = pointA.x - pointB.x;
		float delta_y = pointA.y - pointB.y;

		return Mathf.Sqrt((delta_x * delta_x) + (delta_y * delta_y));
	}

	/// <summary>
	/// Adjusts the length of the connector.
	/// </summary>
	/// <param name="x">The x coordinate offset.</param>
	/// <param name="y">The y coordinate offset.</param>
	void AdjustLength(float x, float y) {
		a.x -= x;
		a.y -= y;
		b.x += x;
		b.y += y;
	}

	public void DebugDraw() {
		Debug.DrawLine(a.AsVector3, b.AsVector3);
	}
}
