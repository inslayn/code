﻿using UnityEngine;
using System.Collections.Generic;

public class VertletRope {

	Vector2 start, end;
	int links;
	float length;
	public float width, gravity = -.2f, stiffness = 10f;
	
	List<VertletPoint> points;
	List<VertletConnector> connectors;

	public VertletRope(Vector2 startPoint, Vector2 endPoint, int numberOfLinks = 10, float connectorLength = 10) {

		start = startPoint;
		end = endPoint;

		length = numberOfLinks * connectorLength;
		links = numberOfLinks;

		points = new List<VertletPoint>(links);
		connectors = new List<VertletConnector>(links - 1);

		Init();
	}

	void Init() {
		int i = 0;

		for(int row = 0, rows = links; row < rows; row++) {
			//Debug.Log(points.Capacity);
			points.Insert(row, new VertletPoint(start));

			if(row > 0)
				connectors.Insert(i++, new VertletConnector(points[row], points[row - 1]));
		}

		Debug.Log(connectors.Count);
	}

	public void FixedUpdate() {
		int count = points != null ? points.Count : 0;
		int i;
		
		// pin the two ends
		points[0].SetPosition(start);

		for(i = 0; i < count; i++) {
			points[i].y += -.2f;
			points[i].UpdatePosition();
		}

		count = connectors.Count;

		for(float stiff = 0, stiffness = 10; stiff < stiffness; stiff++) {
			for(i = 0; i < count; i++) {
				connectors[i].UpdateLength();
			}
		}
	}
}
