using UnityEngine;
using System.Collections.Generic;

public class VertletCloth : MonoBehaviour {

	const int defaultColumns = 30, defaultRows = 25;
	int COLUMNS = defaultColumns, ROWS = defaultRows;
	public int numColumns = defaultColumns, numRows = defaultRows;

	List<VertletPoint> points;
	List<VertletConnector> connectors;
	public float width, gravity = -.2f, stiffness = 10f;

	// Use this for initialization
	void Start () {
		Init();
	}

	void Init() {
		points = new List<VertletPoint>(COLUMNS * ROWS);
		connectors = new List<VertletConnector>((COLUMNS - 1) * ROWS + (ROWS - 1) * COLUMNS);

		int i = 0;

		for(int row = 0, rows = ROWS; row < rows; row++) {
			for(int column = 0, columns = COLUMNS; column < columns; column++) {
				//Debug.Log(points.Capacity);
				points.Insert(row * COLUMNS + column, new VertletPoint(column * 10, row * 10));

				if(column > 0)
					connectors.Insert(i++, new VertletConnector(points[row * COLUMNS + column - 1], points[row * COLUMNS + column]));

				if(row > 0)
					connectors.Insert(i++, new VertletConnector(points[row * COLUMNS + column], points[(row - 1) * COLUMNS + column]));
			}
		}

		width = COLUMNS * 10 + 150;
	}

	void FixedUpdate() {

		if (numColumns > 0 && numColumns != COLUMNS || numRows > 0 && numRows != ROWS) {
			
			COLUMNS = numColumns;
			ROWS = numRows;

			for(int p = 0, pi = points.Count; p < pi; p++)
				points[p].Cleanup();

			Init();
		}

		int count = points != null ? points.Count : 0;
		int i;

		// pin the two corners
		points[0].SetPosition(0, 0);
		points[COLUMNS - 1].SetPosition(width, 0);

		for(i = COLUMNS; i < count; i++) {
			points[i].y += this.gravity;
			points[i].UpdatePosition();
		}

		count = connectors.Count;

		for(float stiff = 0, stiffness = this.stiffness; stiff < stiffness; stiff++) {
			for(i = 0; i < count; i++) {
				connectors[i].UpdateLength();
			}
		}

		if(Input.GetMouseButton(0)) {
			width = Input.mousePosition.x;
		}

		if(Input.GetMouseButton(1)) {
			points[points.Count - 1].SetPosition(width, 0);
		}
	}
}
