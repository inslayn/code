//
//  UnityAlertView.h
//  Unity-iPhone
//
//  Created by Nick McVroom on 06/06/2012.
//  Copyright (c) 2012 inslayn in ya membrane (www.inslayn.co). All rights reserved.
//
//  Enables callback methods to be attached to Cocoa's UIAlertView.  Only for Unity iPhone projects.

#import <Foundation/Foundation.h>

@interface UnityAlertView : NSObject <UIAlertViewDelegate> {
    NSString* cancelCallbackObject;
    NSString* cancelCallbackMethod;
    NSString* confirmCallbackObject;
    NSString* confirmCallbackMethod;
    
    UIAlertView* unityAlertView;
}
@property(copy) NSString* cancelCallbackObject;
@property(copy) NSString* cancelCallbackMethod;
@property(copy) NSString* confirmCallbackObject;
@property(copy) NSString* confirmCallbackMethod;

-(void)showUnityConfirmView:(NSString*)heading message:(NSString*)msg cancelButtonString:(NSString*)cancelButtonText confirmButtonString:(NSString*)confirmButtonText;
-(void)showUnityInfoView:(NSString *)heading message:(NSString *)msg cancelButtonString:(NSString *)cancelButtonText;

@end
