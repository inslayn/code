//
//  UnityAlertView.m
//  Unity-iPhone
//
//  Created by Nick McVroom on 06/06/2012.
//  Copyright (c) 2012 inslayn in ya membrane (www.inslayn.co). All rights reserved.
//
//  Enables callback methods to be attached to Cocoa's UIAlertView.  Only for Unity iPhone projects.

#import "UnityAlertView.h"

@implementation UnityAlertView

@synthesize cancelCallbackObject;
@synthesize cancelCallbackMethod;
@synthesize confirmCallbackObject;
@synthesize confirmCallbackMethod;

-(void)showUnityConfirmView:(NSString*)heading message:(NSString*)msg cancelButtonString:(NSString*)cancelButtonText confirmButtonString:(NSString*)confirmButtonText {
    unityAlertView = [[UIAlertView alloc] initWithTitle: heading
                                           message: msg
                                          delegate: self
                                 cancelButtonTitle: cancelButtonText
                                 otherButtonTitles: confirmButtonText, nil];
    [unityAlertView show];
}

-(void)showUnityInfoView:(NSString *)heading message:(NSString *)msg cancelButtonString:(NSString *)cancelButtonText {
    unityAlertView = [[UIAlertView alloc] initWithTitle: heading
                                                message: msg
                                               delegate: self
                                      cancelButtonTitle: cancelButtonText
                                      otherButtonTitles: nil];
    [unityAlertView show];
}

-(void) alertView: (UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        //NSLog(@"The button was pressed, firing %@ : %@", cancelCallbackObject, cancelCallbackMethod);
        UnitySendMessage([cancelCallbackObject UTF8String], [cancelCallbackMethod UTF8String], "");
        [unityAlertView release];
    }
    else {
        //NSLog(@"The other button was pressed, firing %@ : %@", confirmCallbackObject, confirmCallbackMethod);
        UnitySendMessage([confirmCallbackObject UTF8String], [confirmCallbackMethod UTF8String], "");
        [unityAlertView release];
    }
}

@end
