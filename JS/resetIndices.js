/*  Loops through all the input elements of a given type and renames them in the correct order.
	Useful for when elements have been inserted dynamically into a page. */
function resetIndices(type, name) {
	var elems = $('input[type="' + type + '"]');

	for (var i = elems.length - 1; i >= 0; i--) {
		elems[i].name = name + "[" + i + "]";
	};
}