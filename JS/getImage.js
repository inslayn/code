/* Looks for an image at a given url */
function getImage(url, callbackfunction) {
	// Get the location of the file
	var location = url.substr(0, url.lastIndexOf("/") + 1);

	// Get the name of the track
	var trackname = getTrackNameFromFilename(getFilenameFromURL(url), false);

	// Perform a GET request for the .png image 
	$.get(location + trackname + ".png")
    .done(function() { // if it exists, pass it to the callback.
		console.log('The Image at ' + location + trackname + ".png" + '  exists.');
		callbackfunction(location + trackname + ".png");
    })
    .fail(function() { // If it doesn't, fall through and do a GET for the .jpg image
		$.get(location + trackname + ".jpg")
	    .done(function() { // if it exists, pass it to the callback.
			console.log('The Image at ' + location + trackname + ".jpg" + '  exists.');
			callbackfunction(location + trackname + ".jpg");
	    })
	    .fail(function() { // If it doesn't, fall through and do a GET for the .jpeg image
			$.get(location + trackname + ".jpeg")
		    .done(function() { // if it exists, pass it to the callback.
				console.log('The Image at ' + location + trackname + ".jpeg" + '  exists.');
				callbackfunction(location + trackname + ".jpeg");
		    })
		    .fail(function() { // If it doesn't, fall through and return the default image
				console.log('The Image at ' + location + trackname + ".png | .jpg | .jpeg" + ' doesn\'t exist, or is you don\'t have access. Please check.');
				callbackfunction(getDefaultCoverImage());
		    });
	    });
    });

	return getDefaultCoverImage();
}

/* Returns a default image */
function getDefaultCoverImage() {
	return "./images/default-audio-cover.png";
}

/* Trims the given URL to get the filename */
function getFilenameFromURL(url){
    return url.split('/').pop();
    //return url.substr(url.lastIndexOf("/") + 1); // This way is faster, but the preceding one looks nicer ;p
}

/* Parses a given filename, and removes the extension, optionally replacing full stops. */
function getTrackNameFromFilename(filename, replaceFullStops){
	if(replaceFullStops) {
		// remove the extension and replace any instances of the full stop (aka 'period' or '.') character with a space
	    return filename.substr(0, filename.lastIndexOf(".")).replace(/\./g, " ");
	}
	else {
		return filename.substr(0, filename.lastIndexOf("."));
	}
}